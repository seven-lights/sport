<?php

namespace app\controllers\cabinet;

use app\extensions\CabinetController;
use app\models\UserTeam;
use yii\filters\AccessControl;

class FinancialController extends CabinetController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['pay'],
                'rules' => [
                    [
                        'actions' => ['pay'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionPay()
    {
        $user_teams = UserTeam::find()
            ->where([
                'user_team.user_id' => \Yii::$app->user->id,
                'user_team.team_accept' => 1,
                'user_team.user_accept' => 1,
                'user_team.is_paid' => 0,
            ])/*->andWhere([
                '>', 'program.start_date', new Expression('NOW()')
            ])*/
		    //@todo возможно, стоит вернуть проверку даты
            ->innerJoinWith('team.program') //используется в представлении
            ->all();

        return $this->render('pay', [
            'user_teams' => $user_teams,
        ]);
    }
}
