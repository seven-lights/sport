<?php

namespace app\controllers\cabinet;

use app\extensions\CabinetController;
use app\models\forms\UserActivityForm;
use app\models\Program;
use app\models\UserActivityVariant;
use Yii;
use yii\db\Expression;
use yii\filters\AccessControl;

class ActivityController extends CabinetController {
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index'],
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
    public function actionIndex() {
	    $model = new UserActivityForm();

        if(\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->refresh();
        }

        /** @var Program $program */
        $program = Program::find()
            ->where(['<=', 'program.start_date', new Expression('NOW()')])
            ->andWhere(['>', 'program.start_date', new Expression('NOW() - INTERVAL program.duration DAY')])
            ->andWhere(['user_team.user_id' => Yii::$app->user->id])
            ->innerJoinWith('teams.userTeams', false)
            ->limit(1)
            ->one();

		if($program) {
			$dates = UserActivityVariant::find()
				->select(['user_activity_variant.date'])
				->distinct()
				->where(['user.id' => Yii::$app->user->id])
				->andWhere(['>=', 'user_activity_variant.date', new Expression('program.start_date')])
				->andWhere(['<', 'user_activity_variant.date', new Expression('program.start_date + INTERVAL program.duration DAY')])
				->andWhere(['team.program_id' => $program->id])
				->innerJoinWith('user.teams.program', false)
				->asArray()
				->column();
		} else {
            $dates = [];
        }

        return $this->render('index', [
            'model' => $model,
            'program_start_date' => $program ? $program->start_date : null,
            'disabledDates' => $dates,
        ]);
    }
}