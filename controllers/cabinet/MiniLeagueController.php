<?php

namespace app\controllers\cabinet;

use app\extensions\CabinetController;
use app\models\forms\MiniLeagueForm;
use app\models\MiniLeague;
use app\models\search\MiniLeagueStatisticsSearch;
use app\models\Team;
use app\models\User;
use app\models\UserMiniLeague;
use general\ext\api\passport\PassportApi;
use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class MiniLeagueController extends CabinetController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'index', 'user-accept', 'user-reject', 'pay', 'team'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'user-accept', 'user-reject', 'pay', 'team'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($id = 0, $code = 0)
    {
        //информаци о текущем состоянии: состоит\не состоит в лиге
        //если не состоит: предложение создать лигу

        $mini_league_now = MiniLeague::findOne($id);
        if($id != 0 && !$mini_league_now) {
            throw new NotFoundHttpException;
        }
        $mini_leagues = MiniLeague::find()
            ->where([
                'user_mini_league.user_id' => Yii::$app->user->id,
                'user_mini_league.accepted' => 1,
            ])
            ->innerJoinWith('userMiniLeagues', false)
            ->all();

        if ($mini_leagues && $id == 0) {
            $this->redirect([
                'cabinet/mini-league',
                'id' => $mini_leagues[0]->id,
                'code' => $code ? $code : null,
            ]);
        }

        //приглашения
        $invites = MiniLeague::find()
            ->where([
                'user_mini_league.user_id' => Yii::$app->user->id,
                'user_mini_league.accepted' => 0,
            ])
            ->innerJoinWith('userMiniLeagues', false)
            ->with('owner')//используется в представлении
            ->all();
        
        $search = new MiniLeagueStatisticsSearch();
        $dataProvider = $search->search(['mini_league_id' => $id]);

        $compare = [];
        if($id != 0) {
            /** @var UserMiniLeague $user_mini_league */
            $user_mini_league = UserMiniLeague::find()
                ->where(['mini_league_id' => $id, 'user_id' => Yii::$app->user->id])
                ->limit(1)
                ->one();

            $compare = User::find()
                ->select([
                    'user.id as user_id',
                    'user.nick',
                    'SUM(user_activity_variant.value * activity_variant.coefficient) as sum',
                    'COUNT(user_activity_variant.date) as count',
                ])
                ->where(['user.id' => [Yii::$app->user->id, $user_mini_league->compare_user_id]])
                ->joinWith('userActivityVariants.activityVariant', false)
                ->groupBy('user.id')
                ->indexBy('user_id')
                ->asArray()
                ->all();

            $compare['user_id'] = Yii::$app->user->id;
            $compare['compare_user_id'] = $user_mini_league->compare_user_id;

            $compare[$compare['user_id']]['is_avatar'] = is_file(Yii::getAlias('@webroot') . User::PATH_TO_AVATARS . $compare['user_id'] . '.jpg');
            $compare[$compare['compare_user_id']]['is_avatar'] = is_file(Yii::getAlias('@webroot') . User::PATH_TO_AVATARS . $compare['compare_user_id'] . '.jpg');
        }

        return $this->render('index', [
            'mini_leagues' => $mini_leagues,
            'invites' => $invites,
            'code' => $code,
            'mini_league_now' => $mini_league_now,
            'dataProvider' => $dataProvider,
            'compare' => $compare,
        ]);
    }

    public function actionCreate()
    {
        //создание лиги
        $model = new MiniLeagueForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($mini_league = $model->create()) {
                return $this->redirect(['cabinet/mini-league', 'id' => $mini_league->id]);
            } else {
                //var_dump($model->errors);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUserAccept($id)
    {
        /** @var UserMiniLeague $model */
        $model = UserMiniLeague::find()
            ->where([
                'mini_league_id' => $id,
                'user_id' => Yii::$app->user->id
            ])
            ->limit(1)
            ->one();
        $model->accepted = 1;
        $model->save();

        return $this->redirect(['cabinet/mini-league', [
            'id' => $id,
            'code' => MiniLeague::INVITE_ACCEPT,
        ]]);
    }

    public function actionUserReject($id)
    {
        /** @var UserMiniLeague $model */
        $model = UserMiniLeague::find()
            ->where([
                'mini_league_id' => $id,
                'user_id' => Yii::$app->user->id
            ])
            ->limit(1)
            ->one();
        $model->delete();

        try {
            /** @var \yii\swiftmailer\Mailer $mailer */
            $mailer = Yii::$app->mailer;

            /** @var MiniLeague $mini_league */
            $mini_league = MiniLeague::findOne($id);
            $passport = PassportApi::passportSearch(['user_id' => $mini_league->owner_id]);

            if ($passport['result'] == 'success') {
                $email_to = $passport['passports'][0]['email'];
                $mailer->compose(['text' => 'mini-league/invite_reject_text'], ['user' => Yii::$app->user->identity, 'mini_league' => $mini_league])
                    ->setFrom(Yii::$app->params['email_from'])
                    ->setTo($email_to)
                    ->setSubject('Приглашение отклонено')
                    ->send();
            }
        } catch (\Exception $e) {
        }

        return $this->redirect(['cabinet/mini-league', [
            'code' => Team::USER_REJECT,
        ]]);
    }

    public function actionDelete($id, $user_id)
    {
        if(!$mini_league = MiniLeague::findOne($id)) {
            throw new NotFoundHttpException;
        }
        if($user_id == Yii::$app->user->id) {
            if(count($mini_league->users) == 1 || $mini_league->owner_id == Yii::$app->user->id) {
                $mini_league->delete();
                $this->redirect(['cabinet/mini-league', 'code' => MiniLeague::DELETE_LEAGUE]);
            } else {
                UserMiniLeague::find()
                    ->where([
                        'mini_league_id' => $id,
                        'user_id' => $user_id
                    ])
                    ->limit(1)
                    ->one()
                    ->delete();
                $this->redirect(['cabinet/mini-league', 'code' => MiniLeague::LEAVE_LEAGUE]);
            }
        }
        if($mini_league->owner_id == Yii::$app->user->id) {
            UserMiniLeague::find()
                ->where([
                    'mini_league_id' => $id,
                    'user_id' => $user_id
                ])
                ->limit(1)
                ->one()
                ->delete();
            $this->redirect(['cabinet/mini-league', 'code' => MiniLeague::DELETE_LEAGUE_MEMBER]);
        } else {
            throw new ForbiddenHttpException;
        }
    }

    public function actionCompare($id, $user_id)
    {
        //$model['user_id'] != Yii::$app->user->id && $compare['compare_user_id'] != $model['user_id']
        if($user_id == Yii::$app->user->id) {
            throw new BadRequestHttpException;
        }

        /** @var UserMiniLeague $user_mini_league */
        $user_mini_league = UserMiniLeague::find()
            ->where([
                'mini_league_id' => $id,
                'user_id' => Yii::$app->user->id,
            ])
            ->limit(1)
            ->one();
        
        if(!$user_mini_league) {
            throw new NotFoundHttpException;
        }

        if($user_id == $user_mini_league->compare_user_id) {
            throw new BadRequestHttpException;
        } else {
            $user_mini_league->compare_user_id = $user_id;
            $user_mini_league->save();
            $this->redirect(['cabinet/mini-league', 'id' => $id, 'code' => MiniLeague::COMPARE]);
        }
    }
}