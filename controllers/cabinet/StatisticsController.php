<?php

namespace app\controllers\cabinet;

use app\extensions\CabinetController;
use app\models\search\TeamStatisticsSearch;
use app\models\search\UserStatisticsSearch;
use Yii;
use yii\filters\AccessControl;

class StatisticsController extends CabinetController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'team', 'admin-user', 'admin-team'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['admin-user', 'admin-team'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->is_admin;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new UserStatisticsSearch();
        $dataProvider = $model->search([]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTeam()
    {
        $model = new TeamStatisticsSearch();
        $dataProvider = $model->search([]);

        return $this->render('team', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAdminUser($program_id)
    {
        $model = new UserStatisticsSearch();
        $dataProvider = $model->search(['program_id' => $program_id]);

        return $this->render('admin-user', [
            'dataProvider' => $dataProvider,
            'program_id' => $program_id,
        ]);
    }

    public function actionAdminTeam($program_id)
    {
        $model = new TeamStatisticsSearch();
        $dataProvider = $model->search(['program_id' => $program_id]);

        return $this->render('admin-team', [
            'dataProvider' => $dataProvider,
            'program_id' => $program_id,
        ]);
    }
}