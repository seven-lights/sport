<?php

namespace app\controllers\api;

use app\extensions\CabinetController;
use app\models\forms\PayForm;
use app\models\Team;
use app\models\UserTeam;
use yii\base\InvalidParamException;
use yii\db\Exception;
use yii\db\Expression;
use yii\web\ForbiddenHttpException;

class FinancialController extends CabinetController
{
    public function actionYamoney()
    {
        $data = \Yii::$app->request->post();
        $hash = $data['notification_type'] . '&'
            . $data['operation_id'] . '&'
            . $data['amount'] . '&'
            . $data['currency'] . '&'
            . $data['datetime'] . '&'
            . $data['sender'] . '&'
            . $data['codepro'] . '&'
            . $data['notification_secret'] . '&'
            . $data['label'];

        $sha1 = hash("sha1", $hash);

        if($sha1 != $data['sha1_hash']) {
            throw new ForbiddenHttpException();
        }

        if($data['withdraw_amount'] != 1000) {
            throw new InvalidParamException();
        }

        $ids = explode(':', $data['label']);
        $team_id = $ids[0];
        $user_ids = explode(',', $ids[1]);

        $user_teams = UserTeam::find()
            ->where([
                'team_id' => $team_id,
                'user_id' => $user_ids,
            ])
            ->all();

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            /** @var UserTeam $user_team */
            foreach ($user_teams as $user_team) {
                $user_team->is_paid = 1;
                $user_team->save();
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}
