<?php

use yii\db\Schema;
use yii\db\Migration;

class m160330_005813_add_is_completed_field_in_team_tbl extends Migration
{
    public function up()
    {
        $this->addColumn('team', 'is_completed', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0');
    }

    public function down()
    {
        echo "m160330_005813_add_is_completed_field_in_team_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
