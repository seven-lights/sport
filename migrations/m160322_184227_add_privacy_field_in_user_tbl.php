<?php

use yii\db\Schema;
use yii\db\Migration;

class m160322_184227_add_privacy_field_in_user_tbl extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'privacy', Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 2');
    }

    public function down()
    {
        echo "m160322_184227_add_privacy_field_in_user_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
