<?php

use yii\db\Schema;
use yii\db\Migration;

class m160320_184004_add_fields_in_user_tbl extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'city', Schema::TYPE_STRING);
        $this->addColumn('user', 'kind_of_work', Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0');
        $this->addColumn('user', 'mode_of_work', Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0');
        $this->addColumn('user', 'change_of_time_zones', Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0');
    }

    public function down()
    {
        echo "m160320_184004_add_fields_in_user_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
