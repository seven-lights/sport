<?php

use yii\db\Schema;
use yii\db\Migration;

class m160220_115914_insert_test_data extends Migration
{
    public function up()
    {
        $this->batchInsert('program', [
            'id',
            'start_date',
            'duration',
            'name',
            'created_at',
            'updated_at',
        ], [
            ['1', '2016-02-01', 100, 'Тестовая программа', time(), time()],
        ]);

        $this->batchInsert('program_activity', [
            'program_id',
            'activity_id',
            'created_at',
            'updated_at',
        ], [
            [1, 1, time(), time()],
            [1, 2, time(), time()],
            [1, 3, time(), time()],
            [1, 4, time(), time()],
            [1, 5, time(), time()],
            [1, 6, time(), time()],
            [1, 7, time(), time()],
            [1, 8, time(), time()],
            [1, 9, time(), time()],
            [1, 10, time(), time()],
        ]);
    }

    public function down()
    {
        echo "m160220_115914_insert_test_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
