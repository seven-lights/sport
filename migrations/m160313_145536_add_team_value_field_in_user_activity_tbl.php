<?php

use yii\db\Schema;
use yii\db\Migration;

class m160313_145536_add_team_value_field_in_user_activity_tbl extends Migration
{
    public function up()
    {
        $this->addColumn('user_activity_variant', 'team_value', Schema::TYPE_INTEGER . ' NOT NULL');
    }

    public function down()
    {
        echo "m160313_145536_add_team_value_field_in_user_activity_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
