<?php

use yii\db\Schema;
use yii\db\Migration;

class m160320_083754_fix_data_in_activity_tbl extends Migration
{
    public function up()
    {
        $this->alterColumn('activity', 'measure', Schema::TYPE_STRING . '(50) NOT NULL');
        $this->update('activity', [
            'name' => 'Отжимание',
            'measure' => 'раз за 3 подхода',
        ], [
            'id' => 1,
        ]);
        $this->update('activity', [
            'measure' => 'раз за 3 подхода',
        ], [
            'id' => 2,
        ]);
        $this->update('activity', [
            'name' => 'Пресс',
            'measure' => 'раз за 3 подхода по 1 минуте',
        ], [
            'id' => 3,
        ]);
    }

    public function down()
    {
        echo "m160320_083754_fix_data_in_activity_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
