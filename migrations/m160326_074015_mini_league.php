<?php

use yii\db\Schema;
use yii\db\Migration;

class m160326_074015_mini_league extends Migration
{
    public function up()
    {
        $this->createTable('mini_league', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(50) NOT NULL',
            'owner_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->addForeignKey('owner_id_FK_mini_league', 'mini_league', 'owner_id', 'user', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('user_mini_league', [
            'mini_league_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'accepted' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',
            'compare_user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->addForeignKey('mini_league_id_FK_user_mini_league', 'user_mini_league', 'mini_league_id', 'mini_league', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('user_id_FK_user_mini_league', 'user_mini_league', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('compare_user_id_FK_user_mini_league', 'user_mini_league', 'compare_user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('user_mini_league_tbl_mini_league_id_user_id_idx', 'user_mini_league', ['mini_league_id', 'user_id'], true);
    }

    public function down()
    {
        echo "m160326_074015_mini_league cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
