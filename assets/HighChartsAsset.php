<?php
namespace app\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HighChartsAsset extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'https://code.highcharts.com/stock/highstock.js',
        'https://code.highcharts.com/stock/modules/exporting.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
