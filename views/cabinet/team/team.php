<?php
use app\assets\HighChartsAsset;
use general\ext\DateHelper;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var \app\models\Team $team */
/** @var array $activities */

HighChartsAsset::register($this);

/*[
    [
        {
            "name":"\u041e\u0442\u0436\u0438\u043c\u0430\u043d\u0438\u044f",
            "data":[
                [Date.parse('2016-03-01'),8],
                [Date.parse('2016-03-02'),8],
                [Date.parse('2016-03-03'),9]
            ]
        }
    ]
]*/
$data = '[';
$activity_ids = [];
foreach ($activities as $activity) {
    if(!in_array($activity['activity_id'], $activity_ids, true)) {
        if($data != '[') {
            $data .= ']},';
        }
        $activity_ids[] = $activity['activity_id'];
        $data .= '{"name":"' . Html::encode($activity['name']) . '","data":[';
    } else {
        $data .= ',';
    }
    $data .= '[Date.parse(\'' . $activity['date'] . '\'),' . round($activity['sum']) . ']';
}
$data .= ']}]';

$this->registerJs(<<<JS
Highcharts.setOptions({
    lang: {
        contextButtonTitle: "Chart context menu",
        decimalPoint: ".",
        downloadJPEG: "Сохранить в формате JPEG",
        downloadPDF: "Сохранить в формате PDF",
        downloadPNG: "Сохранить в формате PNG",
        downloadSVG: "Сохранить в формате SVG",
        drillUpText: "Назад к {series.name}",
        loading: "Загрузка...",
        months: [ "Январь" , "Февраль" , "Март" , "Апрель" , "Май" , "Июнь" , "Июль" , "Август" , "Сентябрь" , "Октябрь" , "Ноябрь" , "Декабрь"],
        noData: "Нет данных",
        numericSymbols: [ "k" , "M" , "G" , "T" , "P" , "E"],
        printChart: "Печать графика",
        resetZoom: "Reset zoom",
        resetZoomTitle: "Reset zoom level 1:1",
        shortMonths: [ "янв" , "фев" , "мар" , "апр" , "май" , "июн" , "июл" , "авг" , "сен" , "окт" , "ноя" , "дек"],
        thousandsSep: " ",
        weekdays: ["Воскресение", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"]
    }
});
$('#container').highcharts({
    title: {
        text: 'Итоги по упражнениям',
    },
    tooltip: {
        dateTimeLabelFormats: {
            millisecond:"%A, %b %e, %H:%M:%S.%L",
            second:"%A, %e %b, %H:%M:%S",
            minute:"%A, %e %b, %H:%M",
            hour:"%A, %e %b, %H:%M",
            day:"%A, %e %b %Y",
            week:"Неделя с %A, %e %b %Y",
            month:"%B %Y",
            year:"%Y"
        }
    },
    chart: {
        type: 'area'
    },
    rangeSelector: {
        selected: 1
    },
    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
            millisecond: '%H:%M:%S.%L',
            second: '%H:%M:%S',
            minute: '%H:%M',
            hour: '%H:%M',
            day: '%e %b',
            week: '%e %b',
            month: '%b %Y',
            year: '%Y'
        }
    },
    yAxis: {
        title: {
            text: 'Баллы'
        },
    },
    plotOptions: {
        area: {
            stacking: 'normal',
        }
    },
    credits: {
        enabled: false
    },
    series: {$data}
});
JS
);

?>
<article class="container">
    <div class="page">
        <h1><?= $team->name ?>
            <small>
                (<?= $team->program->name ?> c
                <?= date('j', $team->program->start_date) ?>
                <?= DateHelper::getMonthMin(date('n', $team->program->start_date)) ?>)
            </small>
        </h1>
        <div id="container" style="width: 100%; height: 500px;"></div>
    </div>
</article>
