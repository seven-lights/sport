<?php
use yii\grid\GridView;
use yii\helpers\Html;

/** @var $model \app\models\forms\UserActivityForm */
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\DataProviderInterface */
$this->title = 'Статистика по участникам';
?>
<article class="container">
	<div class="page">
		<h1><?= $this->title ?> <small><a href="<?= Yii::$app->urlManager->createUrl('cabinet/statistics/team') ?>">командная статистика</a></small></h1>

		<?= $this->render('_user', [
			'dataProvider' => $dataProvider,
		]) ?>
	</div>
</article>
