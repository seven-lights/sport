<?php
/** @var integer $program_id */
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\DataProviderInterface */

$this->title = 'Статистика по участникам';
?>
<article class="container">
    <div class="page">
        <h1><?= $this->title ?>
            <small>
                <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/statistics/team', 'program_id' => $program_id]) ?>">
                    командная статистика</a>
            </small>
        </h1>

        <?= $this->render('_user', [
            'dataProvider' => $dataProvider,
        ]) ?>
    </div>
</article>
