<?php
/** @var integer $program_id */
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\DataProviderInterface */

$this->title = 'Статистика команд';
?>
<article class="container">
    <div class="page">
        <h1><?= $this->title ?>
            <small>
                <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/statistics/index', 'program_id' => $program_id]) ?>">
                    общая статистика</a>
            </small>
        </h1>

        <?= $this->render('_team', [
            'dataProvider' => $dataProvider,
        ]) ?>
    </div>
</article>
