<?php
/* @var $this yii\web\View */
/* @var boolean $is_avatar */
use app\models\User;

$this->title = 'Главная';
?>
<article class="container-fluid">
    <div class="dashboard">
        <section class="col-xs-12 col-lg-6 pull-right main">
            <div class="dashboard-wrap">
                <div class="dashboard-image">
                    <img
                        src="https://gccmarketing.blob.core.windows.net/event/2015/logged-in-homepage/PAR0144_Journey_Continues_Mini_League_hpd_1280x720.jpg">
                </div>
                <h2>Вас ждут новые приключения с GCC…</h2>
            </div>
        </section>
        <section class="col-xs-12 col-lg-6 user-info">
            <div class="dashboard-wrap">
                <div class="row">
                    <div class="hidden-xs hidden-sm col-md-3 photo">
                        <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => Yii::$app->user->id]) ?>">
                            <img class="media-object"
                                 src="<?= User::PATH_TO_AVATARS . ($is_avatar ? Yii::$app->user->id : 'no') . '.jpg' ?>">
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-9 info">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => Yii::$app->user->id]) ?>" class="username"><i
                                        class="fa fa-user fa-pull-left"></i><?= Yii::$app->user->identity->nick ?></a>
                            </div>
                            <div class="col-xs-6 team">
                                <?php
                                /** @var \app\models\Team $team */
                                if ($team): ?>
                                    <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/team/team', 'id' => $team->id]) ?>"
                                       class="team-name"><i
                                            class="fa fa-users fa-pull-left"></i><?= $team->name ?></a>
                                <?php else: ?>
                                    <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/team/create']) ?>"><i
                                            class="fa fa-user-plus fa-pull-left"></i>Создать команду</a>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="name col-xs-6">Среднее количество шагов</div>
                            <div class="name col-xs-6">Личное достижение</div>
                        </div>
                        <div class="row">
                            <div class="value col-xs-6"><a
                                    href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => Yii::$app->user->id]) ?>"><?= /* @var float $average */
                                    (int)$average ?></a></div>
                            <div class="value col-xs-6"><a
                                    href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => Yii::$app->user->id]) ?>"><?= /* @var float $maximum */
                                    (int)$maximum ?></a></div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <section class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="dashboard-wrap">
                <div class="dashboard-image">
                    <a href="/steps" class="track-href"><img alt="Подробнее..." class="img-stretch"
                                                             src="https://static.gettheworldmoving.com/content/assets/img/participant/home/modules/step-entry-is-back.jpg"
                                                             title="Подробнее..."/></a>
                </div>
                <header>
                    <a href="/steps" class="track-href">Введите свои шаги.</a>
                </header>
            </div>
        </section>
        <section class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="dashboard-wrap">
                <div class="dashboard-image">
                    <img alt="DAY 100" src="https://gccmarketing.blob.core.windows.net/event/2015/video/day-100.jpg?"
                         class="img-responsive"/>
                </div>
                <header>
                    <a href="/gcctv">100 дней, 100 причин прибавить темп…</a>
                </header>
            </div>
        </section>
        <section class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="dashboard-wrap">
                <div class="dashboard-image">
                    <a href="/stories?homepagepromo=" class="track-href"><img alt="Подробнее..." class="img-stretch"
                                                                              src="https://static.gettheworldmoving.com/content/assets/img/participant/home/modules/meet-your-community.jpg"
                                                                              title="Подробнее..."/></a>
                </div>
                <header>
                    <a href="/stories?homepagepromo=" class="track-href">Посетите сайт нашего глобального
                        онлайн-сообщества.</a>
                </header>
            </div>
        </section>
        <section class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="dashboard-wrap">
                <div class="dashboard-image">
                    <a href="https://www.facebook.com/GlobalCorporateChallenge" target=&#39;_blank&#39;
                       class="track-href"><img alt="Подробнее..." class="img-stretch"
                                               src="https://static.gettheworldmoving.com/content/assets/img/participant/home/modules/facebook.jpg"
                                               title="Подробнее..."/></a>
                </div>
                <header>
                    <a href="https://www.facebook.com/GlobalCorporateChallenge" target=&#39;_blank&#39;
                       class="track-href">Присоединитесь к группе GCC в Facebook</a>
                </header>
            </div>
        </section>
        <section class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="dashboard-wrap">
                <div class="dashboard-image">
                    <a href="https://instagram.com/gcc_crew/" target=&#39;_blank&#39; class="track-href"><img
                            alt="Подробнее..." class="img-stretch"
                            src="https://static.gettheworldmoving.com/content/assets/img/participant/home/modules/instagram.jpg"
                            title="Подробнее..."/></a>
                </div>
                <header>
                    <a href="https://instagram.com/gcc_crew/" target=&#39;_blank&#39; class="track-href">Поделитесь с
                        нами своими идеями в GCC Ideas Lab</a>
                </header>
            </div>
        </section>
        <section class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="dashboard-wrap">
                <div class="dashboard-image">
                    <a href="http://support.gettheworldmoving.com/home" target=&#39;_blank&#39; class="track-href"><img
                            alt="Подробнее..." class="img-stretch"
                            src="https://static.gettheworldmoving.com/content/assets/img/participant/home/modules/frequently-asked-questions.jpg"
                            title="Подробнее..."/></a>
                </div>
                <header>
                    <a href="http://support.gettheworldmoving.com/home" target=&#39;_blank&#39; class="track-href">Есть
                        вопросы?</a>
                </header>
            </div>
        </section>
        <section class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="dashboard-wrap">
                <div class="dashboard-image">
                    <a href="/ideas" target=&#39;_blank&#39; class="track-href"><img alt="Подробнее..."
                                                                                     class="img-stretch"
                                                                                     src="https://static.gettheworldmoving.com/content/assets/img/participant/home/modules/ideas-lab.jpg"
                                                                                     title="Подробнее..."/></a>
                </div>
                <header>
                    <a href="/ideas" target=&#39;_blank&#39; class="track-href">Присоединяйтесь к нам в Инстаграме
                        @GCC_Crew</a>
                </header>
            </div>
        </section>
        <section class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="dashboard-wrap">
                <div class="dashboard-image">
                    <a href="http://us.shop.gettheworldmoving.com/" target=&#39;_blank&#39; class="track-href"><img
                            alt="Подробнее..." class="img-stretch"
                            src="https://static.gettheworldmoving.com/content/assets/img/participant/home/modules/gcc-shop.jpg"
                            title="Подробнее..."/></a>
                </div>
                <header>
                    <a href="http://us.shop.gettheworldmoving.com/" target=&#39;_blank&#39; class="track-href">Выйдите
                        на старт в фирменной футболке с символикой GCC</a>
                </header>
            </div>
        </section>
    </div>
</article>