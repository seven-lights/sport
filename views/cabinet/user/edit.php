<?php
use app\models\User;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\TeamForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $number integer */

$this->title = 'Изменение профиля';
?>
<article class="container">
	<div class="page">
		<h1><?= $this->title ?></h1>

		<div class="form">
			<?php $form = ActiveForm::begin([
				'id' => 'post-form',
				'fieldConfig' => [
					'template' => '<div class="form-row">{label}{input}{error}</div>',
				],
				'enableClientValidation' => true,
                'options' => [
                    'enctype' => 'multipart/form-data'
                ]
			]); ?>

            <?= $form->field($model, 'first_name')->textInput(['maxlength' => 50]) ?>

            <?= $form->field($model, 'last_name')->textInput(['maxlength' => 50]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => 50]) ?>

            <?= $form->field($model, 'phone')->textInput(['maxlength' => 50]) ?>

            <?= $form->field($model, 'city')->textInput(['maxlength' => 50]) ?>

            <?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
                'language' => 'ru',
                'dateFormat' => 'php:d.m.Y',
                'clientOptions' => [
                    'yearRange' => 'c-100:c',
                    'changeMonth' => true,
                    'changeYear' => true,
                ],
                'options' => [
                    'style' => 'z-index: 2; position: relative;',
                    'class' => 'form-control',
                ],
            ]) ?>

            <?= $form->field($model, 'sex')->dropDownList(['Женский', 'Мужской']) ?>

			<div class="form-group">
				<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-block btn-lg']) ?>
			</div>

			<?php ActiveForm::end(); ?>

		</div>
	</div>
</article>