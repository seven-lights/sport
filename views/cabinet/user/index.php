<?php
use app\assets\HighChartsAsset;
use app\models\User;
use general\ext\DateHelper;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var \app\models\Team $team */
/** @var array $activities */

HighChartsAsset::register($this);

/*[
    [
        {
            "name":"\u041e\u0442\u0436\u0438\u043c\u0430\u043d\u0438\u044f",
            "data":[
                [Date.parse('2016-03-01'),8],
                [Date.parse('2016-03-02'),8],
                [Date.parse('2016-03-03'),9]
            ]
        }
    ]
]*/
if ($activities) {
    $data = '[';
    $activity_ids = [];
    foreach ($activities as $activity) {
        if (!in_array($activity['activity_id'], $activity_ids, true)) {
            if ($data != '[') {
                $data .= ']},';
            }
            $activity_ids[] = $activity['activity_id'];
            $data .= '{"name":"' . Html::encode($activity['name']) . '","data":[';
        } else {
            $data .= ',';
        }
        $data .= '[Date.parse(\'' . $activity['date'] . '\'),' . round($activity['sum']) . ']';
    }
    $data .= ']}]';

    $this->registerJs(<<<JS
Highcharts.setOptions({
    lang: {
        contextButtonTitle: "Chart context menu",
        decimalPoint: ".",
        downloadJPEG: "Сохранить в формате JPEG",
        downloadPDF: "Сохранить в формате PDF",
        downloadPNG: "Сохранить в формате PNG",
        downloadSVG: "Сохранить в формате SVG",
        drillUpText: "Назад к {series.name}",
        loading: "Загрузка...",
        months: [ "Январь" , "Февраль" , "Март" , "Апрель" , "Май" , "Июнь" , "Июль" , "Август" , "Сентябрь" , "Октябрь" , "Ноябрь" , "Декабрь"],
        noData: "Нет данных",
        numericSymbols: [ "k" , "M" , "G" , "T" , "P" , "E"],
        printChart: "Печать графика",
        resetZoom: "Reset zoom",
        resetZoomTitle: "Reset zoom level 1:1",
        shortMonths: [ "янв" , "фев" , "мар" , "апр" , "май" , "июн" , "июл" , "авг" , "сен" , "окт" , "ноя" , "дек"],
        thousandsSep: " ",
        weekdays: ["Воскресение", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"]
    }
});
$('#container').highcharts({
    title: {
        text: 'Итоги по упражнениям',
    },
    tooltip: {
        dateTimeLabelFormats: {
            millisecond:"%A, %b %e, %H:%M:%S.%L",
            second:"%A, %e %b, %H:%M:%S",
            minute:"%A, %e %b, %H:%M",
            hour:"%A, %e %b, %H:%M",
            day:"%A, %e %b %Y",
            week:"Неделя с %A, %e %b %Y",
            month:"%B %Y",
            year:"%Y"
        }
    },
    chart: {
        type: 'area'
    },
    rangeSelector: {
        selected: 1
    },
    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
            millisecond: '%H:%M:%S.%L',
            second: '%H:%M:%S',
            minute: '%H:%M',
            hour: '%H:%M',
            day: '%e %b',
            week: '%e %b',
            month: '%b %Y',
            year: '%Y'
        }
    },
    yAxis: {
        title: {
            text: 'Баллы'
        },
    },
    plotOptions: {
        area: {
            stacking: 'normal',
        }
    },
    credits: {
        enabled: false
    },
    series: {$data}
});
JS
    );
}
?>
<article class="container">
    <div class="page">
        <!--h1><?= $team->name ?>
            <small>
                (<?= $team->program->name ?> c
                <?= date('j', $team->program->start_date) ?>
                <?= DateHelper::getMonthMin(date('n', $team->program->start_date)) ?>)
            </small>
        </h1-->
        <div>&nbsp;</div>
        <section class="col-xs-12 user-info">
            <div class="dashboard-wrap">
                <div class="row">
                    <div class="hidden-xs hidden-sm col-md-3 photo">
                        <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => $model->id]) ?>">
                            <img class="media-object" src="<?= /* @var boolean $is_avatar */
                            User::PATH_TO_AVATARS . ($is_avatar ? $model->id : 'no') . '.jpg' ?>">
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-9 info">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => $model->id]) ?>"
                                   class="username"><i class="fa fa-user fa-pull-left"></i><?= $model->nick ?></a>
                            </div>
                            <div class="col-xs-6 team">
                                <?php
                                /** @var \app\models\Team $team */
                                if ($team): ?>
                                    <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/team/team', 'id' => $team->id]) ?>"
                                       class="team-name"><i class="fa fa-users fa-pull-left"></i><?= $team->name ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="name col-xs-6">Среднее количество шагов</div>
                            <div class="name col-xs-6">Личное достижение</div>
                        </div>
                        <div class="row">
                            <div class="value col-xs-6"><a
                                    href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => $model->id]) ?>"><?= /* @var float $average */
                                    round($average) ?></a></div>
                            <div class="value col-xs-6"><a
                                    href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => $model->id]) ?>"><?= /* @var float $maximum */
                                    round($maximum) ?></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <?php if ($activities): ?>
            <div id="container" style="width: 100%; height: 500px;"></div>
        <?php endif; ?>
    </div>
</article>
