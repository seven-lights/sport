<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\MiniLeagueForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Создать мини-лигу';
?>
<article class="container">
    <div class="page">
        <h1><?= $this->title ?></h1>

        <div class="form">
            <?php $form = ActiveForm::begin([
                'id' => 'post-form',
                'fieldConfig' => [
                    'template' => '<div class="form-row">{label}{input}{error}</div>',
                ],
                'enableClientValidation' => true,
            ]); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>

            <h2>Состав лиги</h2>

            <?php $this->registerJs('
            var template = "' . addslashes(strtr($form->field($model, 'emails[0]')->textInput(), ["\n" => '', "\r" => ''])) . '";
            var num = $("div[class*=field-minileagueform-emails]").length;
            eventEmailField();
            
            function insertEmailField() {
                $("div[class*=field-minileagueform-emails-" + (num - 1) + "]").after(template.replace(/0/g, num));
                num = $("div[class*=field-minileagueform-emails]").length;
                eventEmailField();
            }
            function eventEmailField() {
                $("input#minileagueform-emails-" + (num - 1)).one("click", function() {
                    insertEmailField();
                });
            }
            ') ?>

            <?php if ($model->emails) {
                foreach ($model->emails as $key => $email) {
                    echo $form->field($model, 'emails[' . $key . ']')->textInput();
                }
            } else {
                echo $form->field($model, 'emails[0]')->textInput();
            } ?>


            <div class="form-group">
                <?= Html::submitButton('Создать', ['class' => 'btn btn-success btn-block btn-lg']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</article>