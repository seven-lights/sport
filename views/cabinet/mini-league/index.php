<?php
use app\models\MiniLeague;
use app\models\Team;
use app\models\User;
use general\ext\DateHelper;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var MiniLeague $mini_league_now */
/** @var integer $code */
/** @var MiniLeague[] $mini_leagues */
/** @var MiniLeague[] $invites */
/** @var $this yii\web\View */
/** @var $dataProvider \yii\data\DataProviderInterface */
$this->title = 'Мини-лиги';

$mini_league_names = [];
foreach ($mini_leagues as $mini_league) {
    $mini_league_names[$mini_league->id] = $mini_league->name;
}
?>
<article class="container">
    <div class="page">
        <h1><?= $this->title ?></h1>
        <?php if ($code): ?>
            <div class="alert alert-success" role="alert"><?= MiniLeague::getMessage($code) ?></div>
        <?php endif; ?>

        <?php if($compare !== []):
            $this->registerCss(<<<CSS
            #user-compare .values, #user-compare .values a {
                color: #585C61;
            }
            #user-compare .names, #user-compare .names a {
                color: #fff;
            }
            #user-compare .photo img {
                max-height: 80px;
                max-width: 100%;
            }
            #user-compare section:first-child > div, #user-compare section:first-child .username {
                padding-right: 10px;
            }
            #user-compare section:last-child > div, #user-compare section:last-child .username {
                padding-left: 10px;
            }
            #user-compare section:first-child {
                border-right: 1px solid #fff;
            }
            #user-compare section:last-child {
                border-left: 1px solid #fff;
            }
            #user-compare .name {
                font-size: 11px;
                text-transform: uppercase;
                color: #565656;
            }
            #user-compare .values {
                padding: 5px;
                background: #e4e4e4;
            }
            #user-compare .values .row:first-child {
                border-top: 1px solid #fff;
                padding-top: 5px;
            }
            #user-compare .values .row:last-child {
                border-bottom: 1px solid #fff;
                padding-bottom: 5px;
            }
            #user-compare .values .value {
                font-weight: bold;
            }
            #user-compare .names {
                background: #56784e;
                padding: 10px;
            }
CSS
            ) ?>

            <div id="user-compare">
                <?php
                $user = $compare[$compare['user_id']];
                ?>
                <section class="col-xs-12 col-sm-6">
                    <div class="row names">
                        <div class="pull-right photo">
                            <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => $compare['user_id']]) ?>">
                                <img class="media-object"
                                     src="<?= User::PATH_TO_AVATARS . ($user['is_avatar'] ? 1 : 'no') . '.jpg' ?>">
                            </a>
                        </div>
                        <div class="pull-right">
                            <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => $compare['user_id']]) ?>"
                               class="username"><?= $user['nick'] ?>
                            </a>
                        </div>
                    </div>
                    <div class="row values">
                        <div class="row">
                            <div class="name col-xs-6">Среднее количество баллов</div>
                            <div class="name col-xs-6">Всего баллов</div>
                        </div>
                        <div class="row">
                            <div class="value col-xs-6">
                                <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => $compare['user_id']]) ?>">
                                    <?= $user['count'] > 0 ? round($user['sum'] / $user['count']) : 0 ?>
                                </a>
                            </div>
                            <div class="value col-xs-6">
                                <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => $compare['user_id']]) ?>">
                                    <?= round($user['sum']) ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>

                <?php
                $compare_user = $compare[$compare['compare_user_id']];
                ?>
                <section class="col-xs-12 col-sm-6">
                    <div class="row names">
                        <div class="pull-left photo">
                            <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => $compare['compare_user_id']]) ?>">
                                <img class="media-object"
                                     src="<?= User::PATH_TO_AVATARS . ($compare_user['is_avatar'] ? 1 : 'no') . '.jpg' ?>">
                            </a>
                        </div>
                        <div class="pull-left">
                            <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => $compare['compare_user_id']]) ?>"
                               class="username"><?= $compare_user['nick'] ?>
                            </a>
                        </div>
                    </div>
                    <div class="row values">
                        <div class="row">
                            <div class="name col-xs-6">Среднее количество баллов</div>
                            <div class="name col-xs-6">Всего баллов</div>
                        </div>
                        <div class="row">
                            <div class="value col-xs-6">
                                <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => $compare['compare_user_id']]) ?>">
                                    <?= $compare_user['count'] > 0 ? round($compare_user['sum'] / $compare_user['count']) : 0 ?>
                                </a>
                            </div>
                            <div class="value col-xs-6">
                                <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/user/index', 'id' => $compare['compare_user_id']]) ?>">
                                    <?= round($compare_user['sum']) ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="clearfix"></div>

        <?php endif; ?>

        <?php
        $this->registerJs('
        $("#mini-league-form").change(function() {
            var id = $(this).val();
            document.location = "' . Yii::$app->urlManager->createUrl(['/cabinet/mini-league', 'id' => 0]) .  '".replace(/0/g, id);
        });
        ');
        $this->registerCss('
            .choose_league {
                margin: 20px 0;
            }
        ');
        ?>
        <div class="choose_league">
            <?= Html::beginForm('', 'post', [
                'class' => 'form-inline',
            ]) ?>
            <?php if ($mini_league_names): ?>
                <?= Html::label('Ваши мини-лиги', 'mini-league-form') ?>
                <?= Html::dropDownList('mini-league', $mini_league_now->id, $mini_league_names, [
                    'class' => 'form-control',
                    'id' => 'mini-league-form',
                ]) ?>
            <?php endif; ?>
            <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/mini-league/create']) ?>" class="btn btn-success">Создать
                мини-лигу?</a>
            <?= Html::endForm() ?>
        </div>

        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}\n{pager}",
            'columns' => [
                //'id',
                [
                    'attribute' => 'nick',
                    'label' => 'Имя',
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::a($data['nick'], ['cabinet/user/index', 'id' => $data['user_id']]);
                    },
                ],
                [
                    'class' => 'yii\grid\DataColumn',
                    'label' => 'Упражнения',
                    'format' => 'raw',
                    'value' => function ($data) {
                        $data = empty($data['activities']) ? [] : $data['activities'];
                        $ret = '';
                        foreach ($data as $v) {
                            $ret .= Html::tag('p', $v['name'] . ': ' . round($v['value']));
                        }
                        return $ret ?: null;
                    },
                ],
                [
                    'class' => 'yii\grid\DataColumn',
                    'label' => 'Баллов',
                    'attribute' => 'sum',
                    'format' => 'integer',
                    'value' => function ($data) {
                        if (empty($data['sum'])) {
                            return null;
                        } else {
                            return round($data['sum']);
                        }
                    },
                ],
                [
                    'class' => 'yii\grid\DataColumn',
                    'label' => 'Среднее количество баллов',
                    'format' => 'integer',
                    'value' => function ($data) {
                        if (empty($data['avg'])) {
                            return null;
                        } else {
                            return round(array_sum($data['avg']) / count($data['avg']));
                        }
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete} {compare}',
                    'urlCreator' => function ($action, $model, $key, $index) use ($mini_league_now) {
                        $params = is_array($key) ? $key : ['user_id' => (string)$key];
                        $params['id'] = $mini_league_now->id;
                        $params[0] = 'cabinet/mini-league/' . $action;

                        return Url::toRoute($params);
                    },
                    'buttons' => [
                        'delete' => function ($url, $model, $key) use ($mini_league_now) {
                            if ($mini_league_now->owner_id == Yii::$app->user->id || $model['user_id'] == Yii::$app->user->id) {
                                $options = [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'aria-label' => Yii::t('yii', 'Delete'),
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',
                                ];
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                            }
                            return '';
                        },
                        'compare' => function ($url, $model, $key) use ($mini_league_now, $compare) {
                            if ($model['user_id'] != Yii::$app->user->id && $compare['compare_user_id'] != $model['user_id']) {
                                $options = [
                                    'title' => 'Сравнить',
                                    'aria-label' => 'Сравнить',
                                ];
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                            }
                            return '';
                        }
                    ],
                ],
            ],
        ]);
        ?>

        <?php
        if ($invites): ?>
            <h2>Приглашения</h2>

            <?php /** @var MiniLeague $invite */
            foreach ($invites as $invite): ?>
                <p>
                    <?= $invite->owner->nick ?> приглашает в мини-лигу <?= $invite->name ?>.
                </p>
                <p>
                    <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/mini-league/user-accept', 'id' => $invite->id]) ?>"
                       class="btn btn-success">Принять</a>
                    <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/mini-league/user-reject', 'id' => $invite->id]) ?>"
                       class="btn btn-warning">Отклонить</a>
                </p>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</article>