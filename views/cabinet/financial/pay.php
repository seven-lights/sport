<?php
/** @var $this yii\web\View */
/** @var array $user_teams */
use general\ext\DateHelper;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

?>
<article class="container">
    <div class="page">
        <h1>Оплата участия</h1>

        <?php if ($user_teams) {
            /** @var \app\models\UserTeam $user_team */
            foreach ($user_teams as $user_team) { ?>
                <h2><?= $user_team->team->name ?>
                    <small>
                        (<?= $user_team->team->program->name ?> c
                        <?= date('j', $user_team->team->program->start_date) ?>
                        <?= DateHelper::getMonthMin(date('n', $user_team->team->program->start_date)) ?>)
                    </small>
                </h2>
                <div class="form">
                    <?php
                    $form = ActiveForm::begin([
                        'id' => 'post-form',
                        'fieldConfig' => [
                            'template' => '<div class="form-row">{label}{input}{error}</div>',
                        ],
                        'action' => 'https://money.yandex.ru/quickpay/confirm.xml'
                    ]);
                    ?>
                    <?= Html::hiddenInput('receiver', '410012123098328') ?>
                    <?= Html::hiddenInput('quickpay-form', 'shop') ?>
                    <?= Html::hiddenInput('targets', 'Оплата на сайте crosschallenge.ru') ?>
                    <?= Html::hiddenInput('paymentType', 'AC') ?>
                    <?= Html::hiddenInput('sum', '1000') ?>
                    <?= Html::hiddenInput('label', $user_team->team_id . ':' . $user_team->user_id) ?>
                    <?= Html::hiddenInput('successURL', 'http://crosschallenge.ru') ?>
                    <p>Стоимость участия: <?= 1000 ?> руб.</p>
                    <div class="form-group">
                        <?= Html::submitButton('Оплатить', ['class' => 'btn btn-success']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            <?php }
        } else { ?>
            <p>
                Вы не состоите ни в одной команде.
            </p>
            <p>
                <a href="<?= Yii::$app->urlManager->createUrl(['cabinet/team/create']) ?>" class="btn btn-success">Создать
                    команду?</a>
            </p>
        <?php } ?>
    </div>
</article>
