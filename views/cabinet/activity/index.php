<?php
use app\models\Activity;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/** @var $model \app\models\forms\UserActivityForm */
/* @var $this yii\web\View */
$this->title = 'Ввод результатов';
?>
<article class="container">
    <div class="page">
        <h1><?= $this->title ?></h1>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="alert alert-info" role="alert">Введите результаты только за выполненные упражнения</div>
                <?php $form = ActiveForm::begin([
                    'id' => 'activityForm',
                    'fieldConfig' => [
                        'template' => '<div class="form-row">{input}</div>',
                    ],
                ]); ?>

                <?= $form->errorSummary($model, ['header' => '', 'class' => 'alert alert-danger']) ?>

                <?= /** @var integer $program_start_date */
                $form->field($model, 'date')->widget(DatePicker::classname(), [
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'clientOptions' => [
                        'minDate' => $program_start_date ? date('d.m.Y', $program_start_date) : 'undefined',
                        'maxDate' => 'today',
                    ],
                    'options' => [
                        'placeholder' => 'Дата',
                        'class' => 'form-control',
                        'style' => 'z-index: 2; position: relative;',
                    ],
                ]) ?>
                <?php
                /** @var array $disabledDates */
                if($disabledDates) {
                    $disabledDates = json_encode($disabledDates);
                    $this->registerJs(<<<JS
$.datepicker.setDefaults({
    beforeShowDay: function (date) {
        var disabledDays = {$disabledDates};
		var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
		m = ++m < 10 ? "0" + m : m;
		d = d < 10 ? "0" + d : d;
		if (jQuery.inArray(y  + '-' + m + '-' + d, disabledDays) != -1) {
			return [false];
		}
		return [true];
	}
});
JS
                    );
                }

                foreach (Activity::getActivities() as $key => $activity) {
                    echo $form->field($model, 'value[' . $key . ']', [
                        'template' => "{label}\n{input}",
                        'options' => [
                            'class' => 'form-group'
                        ],
                        'labelOptions' => [
                            'label' => $activity,
                        ],
                        'inputOptions' => [
                            'class' => 'form-control',
                        ]
                    ]);
                }
                ?>
                <?= Html::submitButton('Подтвердить', [
                    'class' => 'btn btn-success btn-block',
                    'data-confirm' => 'Подтвердите, что внесены полные данные. После подтверждения исправить ввод за эту дату будет нельзя',
                ]) ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</article>
