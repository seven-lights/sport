<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_team".
 *
 * @property integer $team_id
 * @property integer $user_id
 * @property integer $team_accept
 * @property integer $user_accept
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $is_paid
 *
 * @property Team $team
 * @property User $user
 */
class UserTeam extends ActiveRecord
{
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'user_id'], 'required'],
            [['team_id', 'user_id', 'team_accept', 'user_accept', 'is_paid'], 'integer'],
            [['team_id', 'user_id'], 'unique', 'targetAttribute' => ['team_id', 'user_id'], 'message' => 'The combination of Team ID and User ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'team_id' => 'Team ID',
            'user_id' => 'User ID',
            'team_accept' => 'Team Accept',
            'user_accept' => 'User Accept',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'is_paid' => 'Is Paid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
