<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "activity_variant".
 *
 * @property integer $id
 * @property double $coefficient
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $sex
 * @property integer $activity_id
 *
 * @property Activity $activity
 * @property ProgramActivityVariant[] $programActivityVariants
 * @property Program[] $programs
 * @property UserActivityVariant[] $userActivityVariants
 */
class ActivityVariant extends \yii\db\ActiveRecord
{
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity_variant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coefficient', 'created_at', 'updated_at'], 'required'],
            [['coefficient'], 'number'],
            [['created_at', 'updated_at', 'sex', 'activity_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coefficient' => 'Coefficient',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'sex' => 'Sex',
            'activity_id' => 'Activity ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::className(), ['id' => 'activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramActivityVariants()
    {
        return $this->hasMany(ProgramActivityVariant::className(), ['activity_variant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrograms()
    {
        return $this->hasMany(Program::className(), ['id' => 'program_id'])->viaTable('program_activity_variant', ['activity_variant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserActivityVariants()
    {
        return $this->hasMany(UserActivityVariant::className(), ['activity_variant_id' => 'id']);
    }
}
