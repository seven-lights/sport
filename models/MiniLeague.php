<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "mini-league".
 *
 * @property integer $id
 * @property string $name
 * @property integer $owner_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $owner
 * @property UserMiniLeague[] $userMiniLeagues
 * @property User[] $users
 */
class MiniLeague extends ActiveRecord
{
    const INVITE_ACCEPT = 1;
    const INVITE_REJECT = 2;
    const LEAVE_LEAGUE = 3;
    const DELETE_LEAGUE = 4;
    const DELETE_LEAGUE_MEMBER = 5;
    const COMPARE = 6;

    public static $messages = [
        self::INVITE_ACCEPT => 'Вы приняли приглашение',
        self::INVITE_REJECT => 'Вы отклонили приглашение',
        self::LEAVE_LEAGUE => 'Вы вышли из мини-лиги',
        self::DELETE_LEAGUE => 'Мини-лига удалена',
        self::DELETE_LEAGUE_MEMBER => 'Член мини-лиги удалён',
        self::COMPARE => 'Сравнение изменено',
    ];

    public static function getMessage($code)
    {
        return isset(self::$messages[$code]) ? self::$messages[$code] : 'Непредвиденное сообщение';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mini_league';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'owner_id'], 'required'],
            [['owner_id'], 'integer'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'owner_id' => 'Owner ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserMiniLeagues()
    {
        return $this->hasMany(UserMiniLeague::className(), ['mini_league_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_mini_league', ['mini_league_id' => 'id']);
    }
}
