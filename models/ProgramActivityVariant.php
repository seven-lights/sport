<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "program_activity_variant".
 *
 * @property integer $program_id
 * @property integer $activity_variant_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ActivityVariant $activityVariant
 * @property Program $program
 */
class ProgramActivityVariant extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'program_activity_variant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['program_id', 'activity_variant_id', 'created_at', 'updated_at'], 'required'],
            [['program_id', 'activity_variant_id', 'created_at', 'updated_at'], 'integer'],
            [['program_id', 'activity_variant_id'], 'unique', 'targetAttribute' => ['program_id', 'activity_variant_id'], 'message' => 'The combination of Program ID and Activity Variant ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'program_id' => 'Program ID',
            'activity_variant_id' => 'Activity Variant ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityVariant()
    {
        return $this->hasOne(ActivityVariant::className(), ['id' => 'activity_variant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram()
    {
        return $this->hasOne(Program::className(), ['id' => 'program_id']);
    }
}
