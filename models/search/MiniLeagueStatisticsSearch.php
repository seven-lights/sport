<?php

namespace app\models\search;

use app\models\UserActivityVariant;
use app\models\UserMiniLeague;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\Sort;
use yii\db\Expression;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class MiniLeagueStatisticsSearch extends Model
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $sort = new Sort([
            'attributes' => [
                'sum' => [
                    'default' => SORT_DESC,
                ],
            ],
        ]);

        $users = UserMiniLeague::find()
            ->select(['user_mini_league.*', 'user.nick'])
            ->where([
                'user_mini_league.mini_league_id' => $params['mini_league_id'],
                'user_mini_league.accepted' => 1,
            ])
            ->innerJoinWith('user', false)
            ->indexBy('user_id')
            ->asArray()
            ->all();

        $activities = UserActivityVariant::find()
            ->select([
                'user.id',
                'user_activity_variant.*',
                new Expression('(user_activity_variant.value * activity_variant.coefficient) as sum'),
                'activity_variant.activity_id activity_id',
            ])
            ->where(['user.id' => array_keys($users)])
            ->innerJoinWith('user')
            ->innerJoinWith('activityVariant.activity') //используется ниже
            ->orderBy($sort->orders)
            ->asArray()
            ->all();

        //var_dump($activities);

        foreach ($activities as $activity) {
            if(!isset($users[ $activity['user_id'] ]['activities'][ $activity['activity_id'] ])) {
                $users[ $activity['user_id'] ]['activities'][ $activity['activity_id'] ]['name'] = $activity['activityVariant']['activity']['name'];
                $users[ $activity['user_id'] ]['activities'][ $activity['activity_id'] ]['value'] = 0;
            }
            if(!isset($users[ $activity['user_id'] ]['sum'])) {
                $users[ $activity['user_id'] ]['sum'] = 0;
            }
            if(!isset($users[ $activity['user_id'] ]['avg'][ $activity['date'] ])) {
                $users[ $activity['user_id'] ]['avg'][ $activity['date'] ] = 0;
            }
            $users[ $activity['user_id'] ]['activities'][ $activity['activity_id'] ]['value'] += $activity['sum'];
            $users[ $activity['user_id'] ]['sum'] += $activity['sum'];
            $users[ $activity['user_id'] ]['avg'][ $activity['date'] ] += $activity['sum'];
        }


        $dataProvider = new ArrayDataProvider([
            'allModels' => $users,
        ]);

        return $dataProvider;
    }
}