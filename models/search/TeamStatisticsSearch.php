<?php

namespace app\models\search;

use app\models\Program;
use app\models\Team;
use app\models\UserActivityVariant;
use general\ext\PartArrayDataProvider;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Expression;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class TeamStatisticsSearch extends Model
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city', 'organization', 'user', 'team'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $sort = new Sort([
            'attributes' => [],
        ]);

        if(isset($params['program_id'])) {
            $program_id = $params['program_id'];
        } else {
            $program_id = Program::find()
                ->select('program.id')
                ->where(['<=', 'program.start_date', new Expression('NOW()')])
                ->andWhere(['>', 'program.start_date', new Expression('NOW() - INTERVAL program.duration DAY')])
                ->andWhere(['user_team.user_id' => Yii::$app->user->id])
                ->innerJoinWith('teams.userTeams', false);
        }

        $query = Team::find()
            ->select(['name', 'id'])
            ->where(['program_id' => $program_id]);
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count]);

        $teams = $query
            ->orderBy($sort->orders)
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->indexBy('id')
            ->asArray()
            ->all();

        $activities = UserActivityVariant::find()
            ->select([
                'user.id',
                'user_activity_variant.*',
                new Expression('(user_activity_variant.team_value * activity_variant.coefficient) as sum'),
                'user_team.team_id team_id',
                'activity_variant.activity_id activity_id',
            ])
            ->where(['user_team.team_id' => array_keys($teams)])
            ->andWhere(['>=', 'user_activity_variant.date', new Expression('program.start_date')])
            ->andWhere(['<', 'user_activity_variant.date', new Expression('program.start_date + INTERVAL program.duration DAY')])
            ->innerJoinWith('user.teams.program', false)
            ->innerJoinWith('activityVariant.activity') //используется ниже
            ->asArray()
            ->all();

        foreach ($activities as $activity) {
            $team_id = $activity['team_id'];

            if(!isset($teams[ $team_id ]['activities'][ $activity['activity_id'] ])) {
                $teams[ $team_id ]['activities'][ $activity['activity_id'] ]['name'] = $activity['activityVariant']['activity']['name'];
                $teams[ $team_id ]['activities'][ $activity['activity_id'] ]['value'] = 0;
            }
            if(!isset($teams[ $team_id ]['sum'])) {
                $teams[ $team_id ]['sum'] = 0;
            }
            if(!isset($teams[ $team_id ]['avg'][ $activity['date'] ])) {
                $teams[ $team_id ]['avg'][ $activity['date'] ] = 0;
            }
            $teams[ $team_id ]['activities'][ $activity['activity_id'] ]['value'] += $activity['sum'];
            $teams[ $team_id ]['sum'] += $activity['sum'];
            $teams[ $team_id ]['avg'][ $activity['date'] ] += $activity['sum'];
        }


        $dataProvider = new PartArrayDataProvider([
            'partModels' => $teams,
            'pagination' => $pagination,
            'sort' => $sort,
        ]);

        return $dataProvider;
    }
}