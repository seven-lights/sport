<?php

namespace app\models\search;

use app\models\Program;
use app\models\Team;
use app\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class ProgramSearch extends Model
{
    public $name;
    public $start_date;
    public $duration;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string'],
            [
                'start_date',
                'date',
                'format' => 'dd.MM.yyyy',
                'timestampAttribute' => 'date',
                'timestampAttributeFormat' => 'yyyy-MM-dd',
                'timestampAttributeTimeZone' => 'Asia/Yekaterinburg',
            ],
            ['duration', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'start_date' => 'Дата старта',
            'duration' => 'Продолжительность',
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Program::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'start_date' => $this->start_date,
            'duration' => $this->duration,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}