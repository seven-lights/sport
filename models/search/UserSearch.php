<?php

namespace app\models\search;

use app\models\Team;
use app\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends Model
{
    public $nick;
    public $sex;
    public $city;
    public $without_city;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sex', 'without_city'], 'boolean'],
            [['nick', 'city'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nick' => 'Имя',
            'sex' => 'Пол',
            'city' => 'Город',
            'without_city' => 'Город не указан',
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sex' => $this->sex,
        ]);

        if($this->without_city) {
            $query->andWhere(['city' => null]);
        } else {
            $query->andFilterWhere(['like', 'city', $this->city]);
        }

        $query->andFilterWhere(['like', 'nick', $this->nick]);

        return $dataProvider;
    }
}