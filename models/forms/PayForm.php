<?php
namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * Team form
 */
class PayForm extends Model
{
	public $sum;
	public $label;

	public function rules()
	{
		return [
            [['sum', 'label'], 'required'],
            ['sum', 'integer'],
            ['label', 'string'],
        ];
	}
	public function attributeLabels()
	{
		return [
			'sum' => 'Стоимость участия',
		];
	}
}
