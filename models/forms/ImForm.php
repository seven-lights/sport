<?php
namespace app\models\forms;

use general\ext\api\passport\PassportApi;
use Yii;
use yii\base\Model;

/**
 * Team form
 */
class ImForm extends Model
{
    public $message;

    public function rules()
    {
        return [
            ['message', 'required'],
            ['message', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'message' => 'Сообщение',
        ];
    }

    public function send($user_ids)
    {
        if ($this->validate()) {
            $user_ids = (array)$user_ids;

            /** @var \yii\swiftmailer\Mailer $mailer */
            $mailer = Yii::$app->mailer;

            foreach ($user_ids as $user_id) {
                $passport = PassportApi::passportSearch(['user_id' => $user_id]);
                if ($passport['result'] == 'success') {
                    try {
                        $email_to = $passport['passports'][0]['email'];
                        $sended = $mailer->compose(['text' => 'user/im_text'], ['user' => Yii::$app->user->identity, 'message' => $this->message])
                            ->setFrom(Yii::$app->params['email_from'])
                            ->setTo($email_to)
                            ->setSubject('Сообщение с сайта CROSSCHALENGE.RU')
                            ->send();
                        if (!$sended) {
                            return false;
                        }
                    } catch (\Exception $e) {
                        return false;
                    }
                }
            }

            return true;
        }
        return false;
    }
}
