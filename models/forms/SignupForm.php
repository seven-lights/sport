<?php
namespace app\models\forms;

use general\ext\api\auth\AuthApi;
use general\ext\api\passport\PassportApi;
use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model {
	public $first_name;
	public $last_name;
	public $father_name;
	public $email;
	public $sex;
	public $password;

	public function rules() {
		return [
			[['first_name', 'last_name'], 'required'],
			[['first_name', 'last_name', 'father_name'], 'string', 'min' => 4, 'max' => 50],

			['sex', 'boolean'],

			['email', 'required'],
			['email', 'email'],
			['email', 'uniqueEmail'],

			['password', 'required'],
			['password', 'string', 'min' => 4, 'max' => 50],
		];
	}

	public function uniqueEmail($attribute, $params) {
		$data = PassportApi::passportSearch(['email' => $this->$attribute]);
		if ($data['result'] == 'success') {
			$this->addError($attribute, 'Введённая вами эл. почта уже зарегистрирована.');
		}
	}

	/**
	 * Signs user up.
	 *
	 * @return boolean
	 */
	public function signup() {

		if ($this->validate()) {
			$user = AuthApi::userSignup([
				'login' => $this->email,
				'password' => $this->password,
			]);

			if($user['result'] == 'success') {
				$passport = PassportApi::passportCreate([
					'first_name' => $this->first_name,
					'last_name' => $this->last_name,
					'father_name' => $this->father_name,
					'email' => $this->email,
					'sex' => $this->sex,
					'user_id' => $user['user']['id'],
				]);
				if($passport['result'] == 'success') {
					return true;
				} else {
					AuthApi::userDelete($user['user']['id']);
				}
			}
		}

		return false;
	}

    public function attributeLabels()
    {
        return [
	        'first_name' => 'Имя',
	        'last_name' => 'Фамилия',
	        'father_name' => 'Отчество',
	        'email' => 'Эл. почта',
	        'sex' => 'Пол',
            'password' => 'Пароль',
        ];
    }
}
