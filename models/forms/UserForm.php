<?php

namespace app\models\forms;

use app\models\User;
use general\controllers\api\Controller;
use general\ext\api\auth\AuthApi;
use general\ext\api\passport\PassportApi;
use Imagine\Image\Box;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * @property string $login
 * @property string $password_old
 * @property string $password
 * @property string $password_repeat
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $city
 * @property string $birthday
 * @property integer $sex
 * @property integer $kind_of_work
 * @property integer $mode_of_work
 * @property integer $change_of_time_zones
 * @property integer $privacy
 * @property UploadedFile $avatar
 */
class UserForm extends Model
{
    public $login;
    public $password_old;
    public $password;
    public $password_repeat;
    public $first_name;
    public $last_name;
    public $email;
    public $phone;
    public $city;
    public $birthday;
    public $sex;
    public $kind_of_work;
    public $mode_of_work;
    public $change_of_time_zones;
    public $privacy;

    public $avatar;

    private $_oldAttributes;
    private $_passportId = 0;

    public function rules()
    {
        return [
            [['login', 'first_name', 'last_name', 'email'], 'required'],
            [['login', 'email'], 'trim'],
            ['password_old', 'required', 'when' => function ($model) {
                return !empty($model->password) || !empty($model->password_repeat);
            }, 'whenClient' => 'function (attribute, value) {
        return ($("#userform-password").val() != "") || ($("#userform-password_repeat").val() != "");
    }'],
            ['password', 'required', 'when' => function ($model) {
                return !empty($model->password_repeat);
            }, 'whenClient' => 'function (attribute, value) {
        return $("#userform-password_repeat").val() != "";
    }'],
            ['password_repeat', 'required', 'when' => function ($model) {
                return !empty($model->password);
            }, 'whenClient' => 'function (attribute, value) {
        return $("#userform-password").val() != "";
    }'],
            ['password', 'compare'],
            [['login', 'first_name', 'last_name', 'phone', 'city'], 'string'],
            ['email', 'email'],
            [
                'birthday',
                'date',
                'max' => date('d.m.Y'),
                'format' => 'php:d.m.Y',
            ],
            ['birthday', 'filter', 'filter' => function ($value) {
                return strtotime($value);
            }],
            ['sex', 'boolean'],
            [['kind_of_work', 'mode_of_work', 'change_of_time_zones', 'privacy'], 'integer'],
            [['kind_of_work', 'mode_of_work', 'change_of_time_zones'], 'default', 'value' => 0],
            ['privacy', 'default', 'value' => User::PRIVACY_OPEN],
            ['avatar', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 2 * 1024 * 1024],
        ];
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'password_old' => 'Старый пароль',
            'password' => 'Новый пароль',
            'password_repeat' => 'Повторите пароль',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'email' => 'Эл. почта',
            'phone' => 'Телефон',
            'city' => 'Город',
            'birthday' => 'Дата рождения',
            'sex' => 'Пол',
            'kind_of_work' => 'Общий характер вашей работы',
            'mode_of_work' => 'Вы работаете',
            'change_of_time_zones' => 'Приходится ли вам ездить в командировки со сменой часовых поясов?',
            'privacy' => 'Приватность',
            'avatar' => 'Фотография',
        ];
    }

    public function save($runValidation = true)
    {
        if ($runValidation) {
            if (!$this->validate()) {
                return false;
            }
        }

        $data = $this->getDirtyAttributes(['sex', 'city', 'kind_of_work', 'mode_of_work', 'change_of_time_zones', 'privacy']);
        if ($data) {
            if (self::$_user->nick != ($this->first_name . ' ' . $this->last_name)) {
                self::$_user->nick = $this->first_name . ' ' . $this->last_name;
            }
            self::$_user->setAttributes($data);
            if (!self::$_user->save()) {
                $this->addErrors(self::$_user->getErrors());
                return false;
            }
        }

        $data = $this->getDirtyAttributes(['login']);
        if ($data) {
            $user_auth = AuthApi::userUpdate(self::$_userId, $data);
            if ($user_auth['result'] == 'error') {
                foreach ($user_auth['errors'] as $error) {
                    if ($error['code'] == Controller::ERROR_ILLEGAL_LOGIN) {
                        $this->addError('login', $error['title']);
                        return false;
                    }
                }
                foreach ($data as $key => $item) {
                    $this->addError($key, 'Ошибка связи с сервером');
                }
                return false;
            }
        }

        $data = $this->getDirtyAttributes(['first_name', 'last_name', 'phone', 'email', 'sex', 'birthday']);
        if ($data) {
            if($this->_passportId) {
                $user_passport = PassportApi::passportUpdate($this->_passportId, $data);
            } else {
                $data = array_merge($data, ['user_id' => self::$_userId]);
                $user_passport = PassportApi::passportCreate($data);
            }
            if ($user_passport['result'] == 'error') {
                foreach ($user_passport['errors'] as $error) {
                    switch ($error['code']) {
                        case Controller::ERROR_ILLEGAL_PASSPORT_FIRSTNAME:
                            $this->addError('first_name', $error['title']);
                            return false;
                        case Controller::ERROR_ILLEGAL_PASSPORT_LASTNAME:
                            $this->addError('last_name', $error['title']);
                            return false;
                        case Controller::ERROR_ILLEGAL_PASSPORT_PHONE:
                            $this->addError('phone', $error['title']);
                            return false;
                        case Controller::ERROR_ILLEGAL_PASSPORT_EMAIL:
                            $this->addError('email', $error['title']);
                            return false;
                        case Controller::ERROR_ILLEGAL_PASSPORT_SEX:
                            $this->addError('sex', $error['title']);
                            return false;
                        case Controller::ERROR_ILLEGAL_PASSPORT_BIRTHDAY:
                            $this->addError('birthday', $error['title']);
                            return false;
                    }
                }
                foreach ($data as $key => $item) {
                    $this->addError($key, 'Ошибка связи с сервером');
                }
                return false;
            }
        }

        if ($this->avatar = UploadedFile::getInstance($this, 'avatar')) {
            $path = Yii::getAlias('@webroot') . User::PATH_TO_AVATARS . self::$_userId . '.jpg';
            $this->avatar->saveAs($path);

            $img = Image::getImagine()->open(Yii::getAlias($path));

            $real_size = $img->getSize();
            $target_size = new Box(1000, 1000);
            $coefficient = 1;

            $coefficient = ($real_size->getWidth() / $target_size->getWidth()) > $coefficient ? $real_size->getWidth() / $target_size->getWidth() : $coefficient;
            $coefficient = ($real_size->getHeight() / $target_size->getHeight()) > $coefficient ? $real_size->getHeight() / $target_size->getHeight() : $coefficient;

            $img->resize(new Box($real_size->getWidth() / $coefficient, $real_size->getHeight() / $coefficient));
            $img->save($path);
        }

        return true;
    }

    private static $_userId;
    /** @var  User $_user */
    private static $_user;

    public static function findOne($user_id = null)
    {
        /** @var User $user */
        if(is_null($user_id)) {
            self::$_userId = Yii::$app->user->id;
            self::$_user = Yii::$app->user->identity;
        } else {
            self::$_userId = $user_id;
            self::$_user = User::findOne($user_id);
        }

        $user_form = new UserForm();

        if (self::$_user) {
            $user_form->sex = (int)self::$_user->sex;
            $user_form->city = self::$_user->city;
            $user_form->kind_of_work = self::$_user->kind_of_work;
            $user_form->mode_of_work = self::$_user->mode_of_work;
            $user_form->change_of_time_zones = self::$_user->change_of_time_zones;
            $user_form->privacy = self::$_user->privacy;
        } else {
            throw new Exception('Пользователь не найден');
        }

        $user_auth = AuthApi::userUpdate(self::$_userId);
        if ($user_auth['result'] == 'success') {
            $user_form->login = $user_auth['user']['login'];
        } else {
            throw new Exception('Пользователь не найден');
        }

        $user_passport = PassportApi::passportSearch(['user_id' => self::$_userId]);
        if ($user_passport['result'] == 'success') {
            $user_form->_passportId = $user_passport['passports'][0]['id'];

            $user_form->first_name = $user_passport['passports'][0]['first_name'];
            $user_form->last_name = $user_passport['passports'][0]['last_name'];
            $user_form->phone = $user_passport['passports'][0]['phone'];
            $user_form->email = $user_passport['passports'][0]['email'];
            //$user_form->sex = $user_passport['passports'][0]['sex'];
            $user_form->birthday = $user_passport['passports'][0]['birthday'];
        }

        $user_form->_oldAttributes = $user_form->getAttributes();

        return $user_form;
    }

    public function getDirtyAttributes($names = null)
    {
        if ($names === null) {
            $names = $this->attributes();
        }
        $names = array_flip($names);
        $attributes = [];
        foreach ($names as $name => $v) {
            if (!array_key_exists($name, $this->_oldAttributes) || $this->$name !== $this->_oldAttributes[$name]) {
                $attributes[$name] = $this->$name;
            }
        }
        return $attributes;
    }
}
