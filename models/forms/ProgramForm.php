<?php
namespace app\models\forms;

use app\models\Program;
use Yii;
use yii\db\Expression;

/**
 * Team form
 */
class ProgramForm extends Program
{
    public function rules()
    {
        return [
            [['start_date', 'duration', 'name'], 'required'],
            [
                'start_date',
                'date',
                'format' => 'dd.MM.yyyy',
                'timestampAttribute' => 'start_date',
                'timestampAttributeFormat' => 'yyyy-MM-dd',
                'timestampAttributeTimeZone' => 'Asia/Yekaterinburg',
            ],
            [['duration'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['start_date', function($attribute, $params) {
                $exist = Program::find()
                    ->where(['<=', 'start_date', new Expression('\'' . $this->start_date . '\' + INTERVAL ' . $this->duration . ' DAY')])
                    ->andWhere(['>=', 'start_date', new Expression('\'' . $this->start_date . '\' - INTERVAL duration DAY')])
                    ->exists();

                if($exist) {
                    $this->addError($attribute, 'Сроки программы пересекаются со сроками уже существующей программы');
                }
            }],
        ];
    }
}
